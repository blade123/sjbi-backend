package com.sanjin.springbootinit.manager;

import javax.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AiManagerTest {

  @Resource
  private  AiManager aiManager;

  @Test
  void doChat() {

//    String answer = aiManager.doChat("邓紫棋");
    String answer = aiManager.doChat(1768286649487675394L,"分析需求：\n"
        + "分析网站的增长情况\n"
        + "原始数据：\n"
        + "日期,用户数\n"
        + "1号,10\n"
        + "2号,20\n"
        + "3号,30");
    System.out.println(answer);
  }
}