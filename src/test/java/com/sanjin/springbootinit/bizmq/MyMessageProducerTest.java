package com.sanjin.springbootinit.bizmq;

import javax.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MyMessageProducerTest {

  @Resource
  private MyMessageProducer myMessageProducer;


  @Test
  void sendMessage() {

    myMessageProducer.sendMessage("code_exchange", "my_routingKey", "你好啊!!");

  }
}