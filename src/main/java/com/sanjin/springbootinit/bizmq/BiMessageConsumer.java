package com.sanjin.springbootinit.bizmq;


import com.rabbitmq.client.Channel;
import com.sanjin.springbootinit.common.ErrorCode;
import com.sanjin.springbootinit.exception.BusinessException;
import com.sanjin.springbootinit.manager.AiManager;
import com.sanjin.springbootinit.model.entity.Chart;
import com.sanjin.springbootinit.service.ChartService;
import com.sanjin.springbootinit.constant.BiMqConstant;
import com.sanjin.springbootinit.constant.CommonConstant;
import javax.annotation.Resource;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class BiMessageConsumer {


  @Resource
  private ChartService chartService;

  @Resource
  private AiManager aiManager;

  //  指定程序监听的消息队列和确认机制
  @SneakyThrows
  @RabbitListener(queues = {BiMqConstant.BI_QUEUE_NAME}, ackMode = "MANUAL")
  public void receiveMessage(String message, Channel channel,
      @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {

    log.info("receiveMessage  message ={}",message);

    if (StringUtils.isBlank(message)) {
      channel.basicNack(deliveryTag, false, false);
      throw new BusinessException(ErrorCode.SYSTEM_ERROR, "消息为空");
    }

    long chartId = Long.parseLong(message);

    Chart chart = chartService.getById(chartId);

    if (chart == null){
//      如果失败消息拒绝
      channel.basicNack(deliveryTag, false, false);
      throw new BusinessException(ErrorCode.SYSTEM_ERROR, "图表为空");
    }


    //      先修改图表任务状态 为 “执行中”。等成功后，修改为”已成功“、保存执行结果；执行失败后，状态修改为”失败“，记录任务失败信息
    Chart updateChart = new Chart();
    updateChart.setId(chart.getId());
    updateChart.setStatus("running");
    boolean b = chartService.updateById(updateChart);
    if (!b) {
      channel.basicNack(deliveryTag, false, false);
      handleChartUpdateError(chart.getId(), "更新图表执行中状态失败");
      return;
    }

    //    2. 调用鱼聪明  ai  SDK ，得到AI响应结果   调用 ai
    String result = aiManager.doChat(CommonConstant.BI_MODEL_ID, buildUserInput(chart));
//    3.  从AI响应结果中，取出需要的信息
    String[] splits = result.split("【【【【【");
    if (splits.length < 3) {
      channel.basicNack(deliveryTag, false, false);
      throw new BusinessException(ErrorCode.SYSTEM_ERROR, "AI生成错误");
    }
    String genChart = splits[1].trim();
    String genResult = splits[2].trim();

    Chart updateChartResult = new Chart();
    updateChartResult.setId(chart.getId());
    updateChartResult.setGenChart(genChart);
    updateChartResult.setGenResult(genResult);
//      todo  定义枚举
    updateChartResult.setStatus("succeed");
    boolean updateResult = chartService.updateById(updateChartResult);
    if (!updateResult) {
      channel.basicNack(deliveryTag, false, false);
      handleChartUpdateError(chart.getId(), "更新图表状态失败");
    }

    // 消息确认
    channel.basicAck(deliveryTag, false);

  }


  private void handleChartUpdateError(long chartId, String execMessage) {
    Chart updateChartResult = new Chart();
    updateChartResult.setId(chartId);
    updateChartResult.setStatus("failed");
    updateChartResult.setExecMessage(execMessage);
    boolean updateResult = chartService.updateById(updateChartResult);
    if (!updateResult) {
      log.error("更新图表失败状态失败" + chartId + "," + execMessage);
    }
  }


  /**
   * 构建用户的输入
   *
   * @param chart
   * @return
   */
  private String buildUserInput(Chart chart) {

    String goal = chart.getGoal();
    String chartType = chart.getChartType();
    String csvData = chart.getChartData();

    // 用户输入
    StringBuilder userInput = new StringBuilder();
    userInput.append("分析需求：").append("\n");

//    拼接分析目标   （加上图表的类型）
    String userGoal = goal;
    if (StringUtils.isNotBlank(chartType)) {
      userGoal += " .  请使用" + chartType;
    }
    userInput.append(userGoal).append("\n");
    userInput.append("原始数据：").append("\n");
    userInput.append("数据：").append(csvData).append("\n");
    return  userInput.toString();
  }

}
