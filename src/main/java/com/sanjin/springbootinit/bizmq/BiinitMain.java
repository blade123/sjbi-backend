package com.sanjin.springbootinit.bizmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.sanjin.springbootinit.constant.BiMqConstant;


/**
 * 用于创建测试程序用到的交换机和队列(只用执行一次)
 */
public class BiinitMain {

  public static void main(String[] args) {
    String EXCHANGE_NAME = BiMqConstant.BI_EXCHANGE_NAME;
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    try {
      Connection connection = factory.newConnection();
      Channel channel = connection.createChannel();
      channel.exchangeDeclare(EXCHANGE_NAME, "direct");
      String queueName = BiMqConstant.BI_QUEUE_NAME;
      channel.queueDeclare(queueName, true, false, false, null);
      channel.queueBind(queueName, EXCHANGE_NAME, BiMqConstant.BI_ROUTING_KEY);
    } catch (Exception e) {

    }


  }

}
