package com.sanjin.springbootinit.manager;


import com.sanjin.springbootinit.exception.BusinessException;
import com.sanjin.springbootinit.common.ErrorCode;
import com.yupi.yucongming.dev.client.YuCongMingClient;
import com.yupi.yucongming.dev.common.BaseResponse;
import com.yupi.yucongming.dev.model.DevChatRequest;
import com.yupi.yucongming.dev.model.DevChatResponse;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class AiManager {

  @Resource
  private YuCongMingClient yuCongMingClient;

  /**
   * AI 对话
   *
   * @param  modeId
   * @param message
   * @return
   */
  public String doChat(long modeId, String message) {
    DevChatRequest devChatRequest = new DevChatRequest();
//    devChatRequest.setModelId(1768286649487675394L);
    devChatRequest.setModelId(modeId);
    devChatRequest.setMessage(message);
    BaseResponse<DevChatResponse> response = yuCongMingClient.doChat(devChatRequest);
    if (response == null) {
      throw new BusinessException(ErrorCode.SYSTEM_ERROR, "AI 响应错误");
    }
    return response.getData().getContent();
  }

}
