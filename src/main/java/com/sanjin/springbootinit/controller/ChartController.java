package com.sanjin.springbootinit.controller;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sanjin.springbootinit.annotation.AuthCheck;
import com.sanjin.springbootinit.bizmq.BiMessageProducer;
import com.sanjin.springbootinit.common.DeleteRequest;
import com.sanjin.springbootinit.common.ErrorCode;
import com.sanjin.springbootinit.common.ResultUtils;
import com.sanjin.springbootinit.constant.CommonConstant;
import com.sanjin.springbootinit.constant.UserConstant;
import com.sanjin.springbootinit.exception.BusinessException;
import com.sanjin.springbootinit.exception.ThrowUtils;
import com.sanjin.springbootinit.manager.AiManager;
import com.sanjin.springbootinit.model.dto.chart.ChartAddRequest;
import com.sanjin.springbootinit.model.dto.chart.ChartEditRequest;
import com.sanjin.springbootinit.model.dto.chart.ChartQueryRequest;
import com.sanjin.springbootinit.model.dto.chart.ChartUpdateRequest;
import com.sanjin.springbootinit.model.entity.Chart;
import com.sanjin.springbootinit.model.entity.User;
import com.sanjin.springbootinit.model.vo.BiResponse;
import com.sanjin.springbootinit.service.ChartService;
import com.sanjin.springbootinit.service.UserService;
import com.sanjin.springbootinit.utils.ExcelUtils;
import com.sanjin.springbootinit.common.BaseResponse;
import com.sanjin.springbootinit.model.dto.chart.GenChartByAiRequest;
import com.sanjin.springbootinit.utils.SqlUtils;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 帖子接口
 *
 * @author wx
 */
@RestController
@RequestMapping("/chart")
@Slf4j
public class ChartController {

  @Resource
  private ChartService chartService;

  @Resource
  private UserService userService;

  @Resource
  private AiManager aiManager;

  @Resource
  private BiMessageProducer biMessageProducer;


  @Resource
  private ThreadPoolExecutor threadPoolExecutor;
  // region 增删改查

  /**
   * 创建
   *
   * @param chartAddRequest
   * @param request
   * @return
   */
  @PostMapping("/add")
  public BaseResponse<Long> addChart(@RequestBody ChartAddRequest chartAddRequest,
      HttpServletRequest request) {
    if (chartAddRequest == null) {
      throw new BusinessException(ErrorCode.PARAMS_ERROR);
    }
    Chart chart = new Chart();
    BeanUtils.copyProperties(chartAddRequest, chart);
    User loginUser = userService.getLoginUser(request);
    chart.setUserId(loginUser.getId());
    boolean result = chartService.save(chart);
    ThrowUtils.throwIf(!result, ErrorCode.OPERATION_ERROR);
    long newChartId = chart.getId();
    return ResultUtils.success(newChartId);
  }

  /**
   * 删除
   *
   * @param deleteRequest
   * @param request
   * @return
   */
  @PostMapping("/delete")
  public BaseResponse<Boolean> deleteChart(@RequestBody DeleteRequest deleteRequest,
      HttpServletRequest request) {
    if (deleteRequest == null || deleteRequest.getId() <= 0) {
      throw new BusinessException(ErrorCode.PARAMS_ERROR);
    }
    User user = userService.getLoginUser(request);
    long id = deleteRequest.getId();
    // 判断是否存在
    Chart oldChart = chartService.getById(id);
    ThrowUtils.throwIf(oldChart == null, ErrorCode.NOT_FOUND_ERROR);
    // 仅本人或管理员可删除
    if (!oldChart.getUserId().equals(user.getId()) && !userService.isAdmin(request)) {
      throw new BusinessException(ErrorCode.NO_AUTH_ERROR);
    }
    boolean b = chartService.removeById(id);
    return ResultUtils.success(b);
  }

  /**
   * 更新（仅管理员）
   *
   * @param chartUpdateRequest
   * @return
   */
  @PostMapping("/update")
  @AuthCheck(mustRole = UserConstant.ADMIN_ROLE)
  public BaseResponse<Boolean> updateChart(@RequestBody ChartUpdateRequest chartUpdateRequest) {
    if (chartUpdateRequest == null || chartUpdateRequest.getId() <= 0) {
      throw new BusinessException(ErrorCode.PARAMS_ERROR);
    }
    Chart chart = new Chart();
    BeanUtils.copyProperties(chartUpdateRequest, chart);
    long id = chartUpdateRequest.getId();
    // 判断是否存在
    Chart oldChart = chartService.getById(id);
    ThrowUtils.throwIf(oldChart == null, ErrorCode.NOT_FOUND_ERROR);
    boolean result = chartService.updateById(chart);
    return ResultUtils.success(result);
  }

  /**
   * 根据 id 获取
   *
   * @param id
   * @return
   */
  @GetMapping("/get")
  public BaseResponse<Chart> getChartById(long id, HttpServletRequest request) {
    if (id <= 0) {
      throw new BusinessException(ErrorCode.PARAMS_ERROR);
    }
    Chart chart = chartService.getById(id);
    if (chart == null) {
      throw new BusinessException(ErrorCode.NOT_FOUND_ERROR);
    }
    return ResultUtils.success(chart);

  }

  /**
   * 分页获取列表（封装类）
   *
   * @param chartQueryRequest
   * @param request
   * @return
   */
  @PostMapping("/list/page")
  public BaseResponse<Page<Chart>> listChartByPage(@RequestBody ChartQueryRequest chartQueryRequest,
      HttpServletRequest request) {
    long current = chartQueryRequest.getCurrent();
    long size = chartQueryRequest.getPageSize();
    // 限制爬虫
    ThrowUtils.throwIf(size > 20, ErrorCode.PARAMS_ERROR);
    Page<Chart> chartPage = chartService.page(new Page<>(current, size),
        getQueryWrapper(chartQueryRequest));
    return ResultUtils.success(chartPage);
  }

  /**
   * 分页获取当前用户创建的资源列表
   *
   * @param chartQueryRequest
   * @param request
   * @return
   */
  @PostMapping("/my/list/page")
  public BaseResponse<Page<Chart>> listMyChartByPage(
      @RequestBody ChartQueryRequest chartQueryRequest, HttpServletRequest request) {
    if (chartQueryRequest == null) {
      throw new BusinessException(ErrorCode.PARAMS_ERROR);
    }
    User loginUser = userService.getLoginUser(request);
    chartQueryRequest.setUserId(loginUser.getId());
    long current = chartQueryRequest.getCurrent();
    long size = chartQueryRequest.getPageSize();
    // 限制爬虫
    ThrowUtils.throwIf(size > 20, ErrorCode.PARAMS_ERROR);
    Page<Chart> chartPage = chartService.page(new Page<>(current, size),
        getQueryWrapper(chartQueryRequest));
    return ResultUtils.success(chartPage);
  }

  // endregion


  /**
   * 编辑（用户）
   *
   * @param chartEditRequest
   * @param request
   * @return
   */
  @PostMapping("/edit")
  public BaseResponse<Boolean> editChart(@RequestBody ChartEditRequest chartEditRequest,
      HttpServletRequest request) {
    if (chartEditRequest == null || chartEditRequest.getId() <= 0) {
      throw new BusinessException(ErrorCode.PARAMS_ERROR);
    }
    Chart chart = new Chart();
    BeanUtils.copyProperties(chartEditRequest, chart);
    User loginUser = userService.getLoginUser(request);
    long id = chartEditRequest.getId();
    // 判断是否存在
    Chart oldChart = chartService.getById(id);
    ThrowUtils.throwIf(oldChart == null, ErrorCode.NOT_FOUND_ERROR);
    // 仅本人或管理员可编辑
    if (!oldChart.getUserId().equals(loginUser.getId()) && !userService.isAdmin(loginUser)) {
      throw new BusinessException(ErrorCode.NO_AUTH_ERROR);
    }
    boolean result = chartService.updateById(chart);
    return ResultUtils.success(result);
  }


  /**
   * 智能分析（同步）
   *
   * @param multipartFile
   * @param genChartByAiRequest
   * @param request
   * @return
   */
  @PostMapping("/gen")
  public BaseResponse<BiResponse> genChartByAi(@RequestPart("file") MultipartFile multipartFile,
      GenChartByAiRequest genChartByAiRequest, HttpServletRequest request) {

    /**
     * 智能接口的实现
     * 1.  构造用户请求（用户消息、csv数据、图表类型等）
     * 2.  调用鱼聪明  ai  SDK ，得到AI响应结果
     * 3.  从AI响应结果中，取出需要的信息
     *  4.  保存图表（保存到数据库中）
     */
//     这个是ai的id
    long biModelId = CommonConstant.BI_MODEL_ID;
    String name = genChartByAiRequest.getName();
//    1.  构造用户请求（用户消息、csv数据、图表类型等）
    String goal = genChartByAiRequest.getGoal();
    //这是类型
    String chartType = genChartByAiRequest.getChartType();
    // 检验目标
    ThrowUtils.throwIf(StringUtils.isBlank(goal), ErrorCode.PARAMS_ERROR, "目标为空");
    // 校验名称
    ThrowUtils.throwIf(StringUtils.isBlank(name) && name.length() > 100, ErrorCode.PARAMS_ERROR,
        "名称过长");

    // 校验文件
    long size = multipartFile.getSize();
    String originalFilename = multipartFile.getOriginalFilename();
//    校验文件大小
    final long ONE_MB = 1024 * 1024L;
    ThrowUtils.throwIf(size > ONE_MB, ErrorCode.PARAMS_ERROR, "文件超过  1M");
//    校验文件后缀   aaa.png
    String suffix = FileUtil.getSuffix(originalFilename);
    final List<String> validFileSuffixList = Arrays.asList("xlsx", "xls");
    ThrowUtils.throwIf(!validFileSuffixList.contains(suffix), ErrorCode.PARAMS_ERROR,
        "文件后缀非法");

//  表示要  先登录
    User loginUser = userService.getLoginUser(request);

    // ai的预设   无需写prompt，直接调用ai
//    final String prompt = "你是一个数据分析师和前端开发专家，接下来我会按照以下固定格式给你提供内容：\n"
//        + "分析需求:\n"
//        + "{数据分析的需求或者目标}\n"
//        + "原始数据:\n"
//        + "{csv格式的原始数据，用,作为分隔符)\n"
//        + "请根据这两部分内容，按照以下指定格式生成内容（此外不要输出任何多余的开头、结尾、注释）\n"
//        + "【【【【\n"
//        + "{前端Echarts V5的option配置对象js代码，合理地将数据进行可视化，不要生成任何多余的内容，比如注释}\n"
//        + "【【【【\n"
//        + "{明确的数据分析结论、越详细越好，不要生成多余的注释}";

// 分析需求：
// 分析网站的增长情况
// 原始数据：
// 日期,用户数
// 1号,10
// 2号,20
// 3号,30
    // 校验chart类型  后缀xlsx
    // 用户输入
    StringBuilder userInput = new StringBuilder();
    userInput.append("分析需求：").append("\n");

//    拼接分析目标   （加上图表的类型）
    String userGoal = goal;
    if (StringUtils.isNotBlank(chartType)) {
      userGoal += " .  请使用" + chartType;
    }
    userInput.append(userGoal).append("\n");
    userInput.append("原始数据：").append("\n");
    // 压缩后的数据
    String csvData = ExcelUtils.excelToCsv(multipartFile);
    userInput.append("数据：").append(csvData).append("\n");
//    2. 调用鱼聪明  ai  SDK ，得到AI响应结果
    String result = aiManager.doChat(biModelId, userInput.toString());
//    3.  从AI响应结果中，取出需要的信息
    String[] splits = result.split("【【【【【");
    if (splits.length < 3) {
      throw new BusinessException(ErrorCode.SYSTEM_ERROR, "AI生成错误");
    }
    String genChart = splits[1].trim();
    String genResult = splits[2].trim();
//    插入数据库
    Chart chart = new Chart();
    chart.setName(name);
    chart.setGoal(goal);
    chart.setChartData(csvData);
    chart.setChartType(chartType);
    chart.setGenChart(genChart);
    chart.setGenResult(genResult);
    chart.setUserId(loginUser.getId());
    boolean saveResult = chartService.save(chart);
    ThrowUtils.throwIf(!saveResult, ErrorCode.SYSTEM_ERROR, "图表保存失败");
    BiResponse biResponse = new BiResponse();
    biResponse.setGenChart(genChart);
    biResponse.setGenResult(genResult);
    biResponse.setChartId(chart.getUserId());
    return ResultUtils.success(biResponse);

  }


  /**
   * 智能分析（异步）
   *
   * @param multipartFile
   * @param genChartByAiRequest
   * @param request
   * @return
   */
  @PostMapping("/gen/async")
  public BaseResponse<BiResponse> genChartByAiAsync(
      @RequestPart("file") MultipartFile multipartFile,
      GenChartByAiRequest genChartByAiRequest, HttpServletRequest request) {

    /*
     * 智能接口的实现
     * 1.  构造用户请求（用户消息、csv数据、图表类型等）
     * 2.  调用鱼聪明  ai  SDK ，得到AI响应结果
     * 3.  从AI响应结果中，取出需要的信息
     *  4.  保存图表（保存到数据库中）
     */
//     这个是ai的id
    long biModelId = CommonConstant.BI_MODEL_ID;
    String name = genChartByAiRequest.getName();
//    1.  构造用户请求（用户消息、csv数据、图表类型等）
    String goal = genChartByAiRequest.getGoal();
    //这是类型
    String chartType = genChartByAiRequest.getChartType();
    // 检验目标  是否为空
    ThrowUtils.throwIf(StringUtils.isBlank(goal), ErrorCode.PARAMS_ERROR, "目标为空");
    // 校验名称  名称长度不能大于100
    ThrowUtils.throwIf(StringUtils.isBlank(name) && name.length() > 100, ErrorCode.PARAMS_ERROR,
        "名称过长");

    // 校验文件
    long size = multipartFile.getSize();
    String originalFilename = multipartFile.getOriginalFilename();
//    校验文件大小   1MB
    final long ONE_MB = 1024 * 1024L;
    ThrowUtils.throwIf(size > ONE_MB, ErrorCode.PARAMS_ERROR, "文件超过  1M");
//    校验文件后缀   aaa.png
    String suffix = FileUtil.getSuffix(originalFilename);
    final List<String> validFileSuffixList = Arrays.asList("xlsx", "xls");
    ThrowUtils.throwIf(!validFileSuffixList.contains(suffix), ErrorCode.PARAMS_ERROR,
        "文件后缀非法");

//  表示要  先登录
    User loginUser = userService.getLoginUser(request);
    // 校验chart类型  后缀xlsx
    // 用户输入
    StringBuilder userInput = new StringBuilder();
    userInput.append("分析需求：").append("\n");

//    拼接分析目标   （加上图表的类型）
    String userGoal = goal;
    if (StringUtils.isNotBlank(chartType)) {
      userGoal += " .  请使用" + chartType;
    }
    userInput.append(userGoal).append("\n");
    userInput.append("原始数据：").append("\n");
    // 压缩后的数据
    String csvData = ExcelUtils.excelToCsv(multipartFile);
    userInput.append("数据：").append(csvData).append("\n");

    //    插入数据库
    Chart chart = new Chart();
    chart.setName(name);
    chart.setGoal(goal);
    chart.setChartData(csvData);
    chart.setChartType(chartType);
    chart.setStatus("wait");
    chart.setUserId(loginUser.getId());
    boolean saveResult = chartService.save(chart);
    ThrowUtils.throwIf(!saveResult, ErrorCode.SYSTEM_ERROR, "图表保存失败");
//    todo  建议处理任务队列满了。怕抛出异常
//   用线程池    异步化
    CompletableFuture.runAsync(() -> {
//      先修改图表任务状态 为 “执行中”。等成功后，修改为”已成功“、保存执行结果；执行失败后，状态修改为”失败“，记录任务失败信息
      Chart updateChart = new Chart();
      updateChart.setId(chart.getId());
      updateChart.setStatus("running");
      boolean b = chartService.updateById(updateChart);
      if (!b) {
//        updateChart.setStatus("failed");
//        throw new BusinessException(ErrorCode.OPERATION_ERROR, "图表状态更改失败");
        handleChartUpdateError(chart.getId(), "更新图表执行中状态失败");
        return;
      }

      //    2. 调用鱼聪明  ai  SDK ，得到AI响应结果   调用 ai
      String result = aiManager.doChat(biModelId, userInput.toString());
//    3.  从AI响应结果中，取出需要的信息
      String[] splits = result.split("【【【【【");
      if (splits.length < 3) {
        throw new BusinessException(ErrorCode.SYSTEM_ERROR, "AI生成错误");
      }
      String genChart = splits[1].trim();
      String genResult = splits[2].trim();

      Chart updateChartResult = new Chart();
      updateChartResult.setId(chart.getId());
      updateChartResult.setGenChart(genChart);
      updateChartResult.setGenResult(genResult);
//      todo  定义枚举
      updateChartResult.setStatus("succeed");
      boolean updateResult = chartService.updateById(updateChartResult);
      if (!updateResult) {
//        throw new BusinessException(ErrorCode.OPERATION_ERROR, "图表状态更改失败");
        handleChartUpdateError(chart.getId(), "更新图表状态失败");
      }
    }, threadPoolExecutor);
    //    bi的返回结果  返回前端
    BiResponse biResponse = new BiResponse();
    biResponse.setChartId(chart.getUserId());
    return ResultUtils.success(biResponse);
  }


  /**
   * 智能分析（异步）消息队列
   *
   * @param multipartFile
   * @param genChartByAiRequest
   * @param request
   * @return
   */
  @PostMapping("/gen/async/mq")
  public BaseResponse<BiResponse> genChartByAiAsyncMq(
      @RequestPart("file") MultipartFile multipartFile,
      GenChartByAiRequest genChartByAiRequest, HttpServletRequest request) {

    /*
     * 智能接口的实现
     * 1.  构造用户请求（用户消息、csv数据、图表类型等）
     * 2.  调用鱼聪明  ai  SDK ，得到AI响应结果
     * 3.  从AI响应结果中，取出需要的信息
     *  4.  保存图表（保存到数据库中）
     */
//     这个是ai的id
    long biModelId = CommonConstant.BI_MODEL_ID;
    String name = genChartByAiRequest.getName();
//    1.  构造用户请求（用户消息、csv数据、图表类型等）
    String goal = genChartByAiRequest.getGoal();
    //这是类型
    String chartType = genChartByAiRequest.getChartType();
    // 检验目标  是否为空
    ThrowUtils.throwIf(StringUtils.isBlank(goal), ErrorCode.PARAMS_ERROR, "目标为空");
    // 校验名称  名称长度不能大于100
    ThrowUtils.throwIf(StringUtils.isBlank(name) && name.length() > 100, ErrorCode.PARAMS_ERROR,
        "名称过长");

    // 校验文件
    long size = multipartFile.getSize();
    String originalFilename = multipartFile.getOriginalFilename();
//    校验文件大小   1MB
    final long ONE_MB = 1024 * 1024L;
    ThrowUtils.throwIf(size > ONE_MB, ErrorCode.PARAMS_ERROR, "文件超过  1M");
//    校验文件后缀   aaa.png
    String suffix = FileUtil.getSuffix(originalFilename);
    final List<String> validFileSuffixList = Arrays.asList("xlsx", "xls");
    ThrowUtils.throwIf(!validFileSuffixList.contains(suffix), ErrorCode.PARAMS_ERROR,
        "文件后缀非法");

//  表示要  先登录
    User loginUser = userService.getLoginUser(request);
    // 校验chart类型  后缀xlsx
    // 用户输入
    StringBuilder userInput = new StringBuilder();
    userInput.append("分析需求：").append("\n");

//    拼接分析目标   （加上图表的类型）
    String userGoal = goal;
    if (StringUtils.isNotBlank(chartType)) {
      userGoal += " .  请使用" + chartType;
    }
    userInput.append(userGoal).append("\n");
    userInput.append("原始数据：").append("\n");
    // 压缩后的数据
    String csvData = ExcelUtils.excelToCsv(multipartFile);
    userInput.append("数据：").append(csvData).append("\n");

    //    插入数据库
    Chart chart = new Chart();
    chart.setName(name);
    chart.setGoal(goal);
    chart.setChartData(csvData);
    chart.setChartType(chartType);
    chart.setStatus("wait");
    chart.setUserId(loginUser.getId());
    boolean saveResult = chartService.save(chart);
    ThrowUtils.throwIf(!saveResult, ErrorCode.SYSTEM_ERROR, "图表保存失败");

    Long newChartId = chart.getId();

//    todo  建议处理任务队列满了。怕抛出异常
    biMessageProducer.sendMessage(String.valueOf(newChartId));
    //    bi的返回结果  返回前端
    BiResponse biResponse = new BiResponse();
    biResponse.setChartId(newChartId);
    return ResultUtils.success(biResponse);
  }

  private void handleChartUpdateError(long chartId, String execMessage) {
    Chart updateChartResult = new Chart();
    updateChartResult.setId(chartId);
    updateChartResult.setStatus("failed");
    updateChartResult.setExecMessage(execMessage);
    boolean updateResult = chartService.updateById(updateChartResult);
    if (!updateResult) {
      log.error("更新图表失败状态失败" + chartId + "," + execMessage);
    }

  }

  /**
   * 获取查询包装类
   *
   * @param chartQueryRequest
   * @return
   */
  private QueryWrapper<Chart> getQueryWrapper(ChartQueryRequest chartQueryRequest) {
    QueryWrapper<Chart> queryWrapper = new QueryWrapper<>();
    if (chartQueryRequest == null) {
      return queryWrapper;
    }
    // chart 需要拼接 chartRequest
    Long id = chartQueryRequest.getId();
    String name = chartQueryRequest.getName();
    String goal = chartQueryRequest.getGoal();
    String chartType = chartQueryRequest.getChartType();
    Long userId = chartQueryRequest.getUserId();
    String sortField = chartQueryRequest.getSortField();
    String sortOrder = chartQueryRequest.getSortOrder();
    // 拼接查询条件
//    eq 是等于
    queryWrapper.eq(id != null && id > 0, "id", id);
    queryWrapper.eq(StringUtils.isNotBlank(goal), "goal", goal);
//    like是模糊匹配
    queryWrapper.like(StringUtils.isNotBlank(name), "name", name);
    queryWrapper.eq(StringUtils.isNotBlank(chartType), "chartType", chartType);
    queryWrapper.eq(ObjectUtils.isNotEmpty(userId), "userId", userId);

    queryWrapper.orderBy(SqlUtils.validSortField(sortField),
        sortOrder.equals(CommonConstant.SORT_ORDER_ASC), sortField);
    return queryWrapper;
  }


}
