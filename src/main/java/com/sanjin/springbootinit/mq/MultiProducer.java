package com.sanjin.springbootinit.mq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.util.Scanner;

public class MultiProducer {

  private static final String TASK_QUEUE_NAME = "multi_queue";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    try (Connection connection = factory.newConnection();
         Channel channel = connection.createChannel()) {
//      durable 参数设置为true，服务器重启队列不丢失    队列持久化
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
//  便于多发消息
      Scanner scanner = new Scanner(System.in);
      while (scanner.hasNext()){
        String message = scanner.nextLine();
        channel.basicPublish("", TASK_QUEUE_NAME,
            //        消息持久化
            MessageProperties.PERSISTENT_TEXT_PLAIN,
            message.getBytes("UTF-8"));
        System.out.println(" [x] Sent '" + message + "'");
      }
//      String message = String.join(" ", argv);
    }
  }

}