package com.sanjin.springbootinit.mq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

public class MultiConsumer {

  private static final String TASK_QUEUE_NAME = "multi_queue";

  public static void main(String[] argv) throws Exception {

//    建立连接
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    final Connection connection = factory.newConnection();

    for (int i = 1; i < 2; i++) {

      final Channel channel = connection.createChannel();
      channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
      System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

//    控制单个消费者的处理任务的积压数       每一个消费者最多同时处理一个任务
      channel.basicQos(1);
//定义如何处理消息
      int finalI = i;
      DeliverCallback deliverCallback = (consumerTag, delivery) -> {
        String message = new String(delivery.getBody(), "UTF-8");
        try {
//          处理工作
          System.out.println(" [x] Received '  " + "编号" + finalI + ":" + message + " ' ");
//        停20 s  模拟机器处理能力有限
//           第二个参数multiple  是指是否要一次性确认所有的历史消息直到当前这条   ---批量确认
          channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
          Thread.sleep(20000);
        } catch (InterruptedException e) {
          e.printStackTrace();
//          批量拒绝    第三个参数是否重新入队：是指是否要一次性确认所有的历史消息直到当前这条
          channel.basicNack(delivery.getEnvelope().getDeliveryTag(), false, false);
        } finally {
          System.out.println(" [x] Done");
          channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        }
      };

//    开启消费
      channel.basicConsume(TASK_QUEUE_NAME, false, deliverCallback, consumerTag -> {
      });

    }
  }


}