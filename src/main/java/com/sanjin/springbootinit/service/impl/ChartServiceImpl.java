package com.sanjin.springbootinit.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sanjin.springbootinit.model.entity.Chart;
import com.sanjin.springbootinit.service.ChartService;
import com.sanjin.springbootinit.mapper.ChartMapper;
import org.springframework.stereotype.Service;

/**
* @author wangxin
* @description 针对表【chart(图表信息表)】的数据库操作Service实现
* @createDate 2024-03-07 23:21:40
*/
@Service
public class ChartServiceImpl extends ServiceImpl<ChartMapper, Chart>
    implements ChartService{

}




