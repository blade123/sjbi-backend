package com.sanjin.springbootinit.config;


import lombok.Data;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "spring.redis")
@Data
public class RedissonConfig {

// 数据库
  private Integer database;

//  地址
  private String host;

//  端口
  private Integer port;

//  密码
  private String password;

  @Bean
  public RedissonClient redissonClient() {
    Config config = new Config();
    config.useSingleServer()
        .setDatabase(database)
        .setAddress("redis://" + host + ":" + port)
//        .setAddress("redis://127.0.0.1:6379")
        .setPassword("123456");
    return Redisson.create(config);
  }


}
