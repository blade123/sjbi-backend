package com.sanjin.springbootinit.common;

import java.io.Serializable;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

/**
 * 通用返回类
 *
 * @param <T>
 * @author wx
 */
@Data
public class BaseResponse<T> implements Serializable {

    private int code;

    private T data;

    private String message;

    public BaseResponse(int code, T data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public BaseResponse(int code, T data) {
        this(code, data, "");
    }

    public BaseResponse(@NotNull ErrorCode errorCode) {
        this(errorCode.getCode(), null, errorCode.getMessage());
    }
}
