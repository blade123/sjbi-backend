package com.sanjin.springbootinit.utils;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * Excel 相关工具类
 *
 * @author wangxin
 */
@Slf4j
public class ExcelUtils {

  /**
   * excel 转 csv
   *
   * @param multipartFile
   * @return
   */

  public static String excelToCsv(MultipartFile multipartFile) {
//    File file = null;
//    try {
//      file = ResourceUtils.getFile("classpath:网站数据.xlsx");
//    } catch (FileNotFoundException e) {
//      throw new RuntimeException(e);
//    }

    // 1、读取数据
    List<Map<Integer, String>> list = null;
    try {
      list = EasyExcel.read(multipartFile.getInputStream())
          .excelType(ExcelTypeEnum.XLSX)
          .sheet()
          .headRowNumber(0)
          .doReadSync();
    } catch (IOException e) {
      log.error("表格处理错误", e);
//      throw new RuntimeException(e);
    }
    if (CollUtil.isEmpty(list)) {
      return "";
    }

    // 转化为scv
    StringBuilder stringBuilder = new StringBuilder();
    // 读取表头
    LinkedHashMap<Integer, String> headerMap = (LinkedHashMap<Integer, String>) list.get(0);
    // 过滤
//    headerMap.values().stream().filter(values ->ObjectUtils.isNotEmpty(values)).collect(Collectors.toList())
    List<String> headerList = headerMap.values().stream().filter(ObjectUtils::isNotEmpty)
        .collect(Collectors.toList());
    stringBuilder.append(StringUtils.join(headerList, ",")).append("\n");
    for (int i = 1; i < list.size(); i++) {
      LinkedHashMap<Integer, String> dataMap = (LinkedHashMap<Integer, String>) list.get(i);
      List<String> dataList = dataMap.values().stream().filter(ObjectUtils::isNotEmpty).collect(
          Collectors.toList());
      stringBuilder.append(StringUtils.join(dataList, ",")).append("\n");
//      System.out.println(StringUtils.join(dataList,","));
    }
//    System.out.println(list);
    return stringBuilder.toString();
  }


//  public static void main(String[] args) {
//    excelToCsv(null);
//  }

}
