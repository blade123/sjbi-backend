package com.sanjin.springbootinit.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sanjin.springbootinit.model.entity.User;

/**
* @author wangxin
* @description 针对表【user(用户)】的数据库操作Mapper
* @createDate 2024-03-07 23:21:40
* @Entity com.yupi.springbootinit.model.entity.User
*/
public interface UserMapper extends BaseMapper<User> {

}




