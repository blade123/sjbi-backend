package com.sanjin.springbootinit.mapper;

import com.sanjin.springbootinit.model.entity.Chart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @description 针对表【chart(图表信息表)】的数据库操作Mapper
* @createDate 2024-03-07 23:21:40
*/
public interface ChartMapper extends BaseMapper<Chart> {

}