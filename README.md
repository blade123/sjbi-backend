# 智能BI

### 项目简介

基于React + Spring Boot + MQ + AIGC的智能数据分析平台。

区别鱼传统BI，用户只需要导入原始数据集、并输入分析分析诉求，就能自动生成可视化图表及分析结论，实现数据分析的降本增效。

在传统的数据分析平台中，如果我们想要分析近一年网站的用户增长趋势，通常需要手动导入数据、选择要分析的字段和图表，并由专业的数据分析刊师完成分析，最后得出结论。然而，本次设计的项目与传统平台有所不同。在这个项目中，用户只需输入想要分析的目标，并上传原始数据，系统将利用自AI自动生成可视化图表和学习的分析结论。这样，即使是对数据分析一窍不通的人也能轻松使用该系统。



AIGC：AI生成内容



### 技术栈

#### 前端    

​	[源码地址](https://gitee.com/blade123/sjbi-frontend.git)

1. React
2. Umi + Ant Design Pro
3. 可视化开发库（Echarts + HighChart + AntV）<可视化会涉及图表的生成>
4. umi openapi 代码生成（自动生成后端调用代码）

#### 后端   

​	[源码地址](https://gitee.com/blade123/sjbi-backend.git)

1. Spring Boot (万用Java后端项目模板，快速搭建基础框架，避免重复写代码)
2. MySQL数据库
3. MyBatis Plus 数据访问框架
4. 消息队列（RabbitMQ）
5. AI能力（Open AI 接口开发）
6. Excel的上传和数据的解析（Easy Excel）
7. Swagger + Knife4j项目接口文档
8. Hutool工具库







